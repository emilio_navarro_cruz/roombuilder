import React, { Component } from 'react';

class Shape extends Component {
  clicked() {
    this.props.shapeClicked(this);
  };

  render() {
    return (
      <a onClick={this.clicked.bind(this)} href="#!" className="collection-item">
        <i className={"" + this.props.iconClass} aria-hidden="true"> </i>
        <div className="secondary-content">
          {" " + this.props.description}
        </div>
      </a>
    );
  };
}

export default Shape;