import React, { Component } from 'react';

class Layers extends Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: true
    };
  }

  visibilityChanged() {
    this.setState({visible: !this.state.visible});

    this.props.changeLayerVisibility(this.props.layerInfo);
  }

  render() {

    return (

      <li>
        <div className="collapsible-header">{this.props.description}</div>
        <div className="collapsible-body padded">
          <a onClick={this.visibilityChanged.bind(this)} className={ this.state.visible ? "waves-effect waves-light btn" : "waves-effect waves-teal btn-flat"} >Visible</a>
        </div>
      </li>

    );
  }
}

export default Layers;