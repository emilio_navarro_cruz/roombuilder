import React, { Component } from 'react';

class Menu extends Component {
  render() {
    return (
    <nav id="RoomViewerNav" >

      <ul id="dropdown1" className="dropdown-content">
        <li><a href="#"><i className="fa fa-circle" aria-hidden="true"></i> New</a></li>
        <li><a href="#"><i className="fa fa-print" aria-hidden="true"></i> Print</a></li>
        <li><a href="#"><i className="fa fa-save" aria-hidden="true"></i> Save</a></li>
        <li><a href="#modal1"><i className="fa fa-save" aria-hidden="true"></i> Generate Order</a></li>
      </ul>

      <ul id="dropdown2" className="dropdown-content">
        <li><a href="#"><i className="fa fa-files-o" aria-hidden="true"></i> Copy</a></li>
        <li><a href="#"><i className="fa fa-clipboard" aria-hidden="true"></i> Paste</a></li>
        <li><a href="#"><i className="fa fa-undo" aria-hidden="true"></i> Undo</a></li>
        <li><a href="#"><i className="fa fa-repeat" aria-hidden="true"></i> Redo</a></li>
      </ul>

      <ul id="dropdown3" className="dropdown-content">
        <li><a href="#"><i className="fa fa-search-plus" aria-hidden="true"></i> Zoom In</a></li>
        <li><a href="#"><i className="fa fa-search-minus" aria-hidden="te"></i> Zoom Out</a></li>
        <li><a href="#"><i className="fa fa-circle" aria-hidden="true"></i> Show Text</a></li>
        <li><a href="#"><i className="fa fa-circle" aria-hidden="true"></i> Show Dimensions</a></li>
      </ul>

      <div id="nav-mobile" className="nav-wrapper orange darken-2">

      <a href="#!" className="brand-logo right">//TODO</a>
        <ul className="left">
          <li><a className="dropdown-button" href="#!" data-activates="dropdown1"> File </a></li>
          <li><a className="dropdown-button" href="#!" data-activates="dropdown2"> Edit </a></li>
          <li><a className="dropdown-button" href="#!" data-activates="dropdown3"> View </a></li>
        </ul>
      </div>
    </nav>

    );
  }
}

export default Menu;
