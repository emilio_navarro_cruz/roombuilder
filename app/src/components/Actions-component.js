import React, { Component } from 'react';
import Layer from './Layer-component';

class ActionMenu extends Component {

  deleteClicked() {
    this.props.onDelete();
  };

  undoClicked() {
    this.props.onUndo();
  };

  redoClicked() {
    this.props.onRedo();
  };

  render() {
    return (
      <div className="card">
        <div className="card-image waves-effect waves-block waves-light">

        </div>
        <div className="card-content">
          <button onClick={this.deleteClicked.bind(this)} className="margined waves-effect waves-teal btn-large blue" >
            Delete
          </button>
          <a href="#!" onClick={this.undoClicked.bind(this)} className="margined waves-effect waves-teal btn-large blue " >
            Undo
          </a>
          <a href="#!" onClick={this.redoClicked.bind(this)} className="margined waves-effect waves-teal btn-large blue" >
            Redo
          </a>
        </div>
      </div>
    );
  }
}

export default ActionMenu;