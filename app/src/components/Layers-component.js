import React, { Component } from 'react';
import Layer from './Layer-component';

class Layers extends Component {
  changeLayerVisibility(layer) {
    this.props.changeLayerVisibility(layer);
  }

  render() {
    var layers = [];

    if (this.props.layers) {
      layers = this.props.layers.map(function (layer) {
        return <Layer changeLayerVisibility={this.changeLayerVisibility.bind(this)} description={layer._id} layerInfo={layer} />
      }.bind(this));
    }


    return (
      <div className="card">
        <div className="card-image waves-effect waves-block waves-light">

        </div>
        <div className="card-content">
          <h4>Layers</h4>

          <ul className="collapsible" data-collapsible="accordion">
            {layers}
          </ul>
        </div>
      </div>
    );
  }
}

export default Layers;