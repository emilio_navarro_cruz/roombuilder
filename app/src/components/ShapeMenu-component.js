import React, { Component } from 'react';
import Shape from './Shape-component';
import '.././App.css';

class ShapeMenu extends Component {

  shapeClicked(shape) {
    this.props.addShape(shape);
  };
  
  render() {
    return (
      <div className="card">
        <div className="card-image waves-effect waves-block waves-light">

        </div>
        <div className="card-content">
          <h4>Shapes</h4>

          <div className="collection scrollable">
            <Shape iconClass={"fa fa-square fa-2x"} id={0} description="Chair" shapeClicked={this.shapeClicked.bind(this)} />
            <Shape iconClass={"fa fa-square fa-2x"} id={1} description="Projector" shapeClicked={this.shapeClicked.bind(this)} />
            <Shape iconClass={"fa fa-square fa-2x"} id={2} description="Lamp" shapeClicked={this.shapeClicked.bind(this)} />
            <Shape iconClass={"fa fa-square fa-2x"} id={3} description="Table" shapeClicked={this.shapeClicked.bind(this)} />
            <Shape iconClass={"fa fa-square fa-2x"} id={4} description="Desk" shapeClicked={this.shapeClicked.bind(this)} />
            <Shape iconClass={"fa fa-square fa-2x"} id={5} description="Phone" shapeClicked={this.shapeClicked.bind(this)} />
            <Shape iconClass={"fa fa-square fa-2x"} id={6} description="Princess Ren" shapeClicked={this.shapeClicked.bind(this)} />
            <Shape iconClass={"fa fa-square fa-2x"} id={7} description="Lotion" shapeClicked={this.shapeClicked.bind(this)} />
            <Shape iconClass={"fa fa-square fa-2x"} id={8} description="Podium" shapeClicked={this.shapeClicked.bind(this)} />
            <Shape iconClass={"fa fa-square fa-2x"} id={9} description="Chair and Table" shapeClicked={this.shapeClicked.bind(this)} />
            <Shape iconClass={"fa fa-square fa-2x"} id={10} description="Round Table" shapeClicked={this.shapeClicked.bind(this)} />
          </div>
        </div>
      </div>
    );
  }
}

export default ShapeMenu;