import React, { Component } from 'react';
import Menu from './components/Menu-component';
import ShapeMenu from './components/ShapeMenu-component';
import LayerMenu from './components/Layers-component';
import ActionMenu from './components/Actions-component';
import $ from "jquery";
var Viewer = require('./Viewer.js');

if (Viewer.__esModule) {
  Viewer = Viewer.default;
}

class App extends Component {
  constructor(props) {
    super(props);

    this.viewer;
    this.hackathonShapes = new HackathonShapes();

    this.state = { showLayersPanel: false };
  }

  changeLayerVisibility(layer) {
    let matchedLayer = null;

    for (var i = 0, len = this.viewer.layers.length; i < len; i++) {
      if (layer._id === this.viewer.layers[i]._id) {
        matchedLayer = layer;
      }
    }

    matchedLayer.visible = !layer.visible;
  }

  addShape(shape) {
    let shapeAdded = false;
    let defaultLayer;

    switch (shape.props.id) {
      case 0: //chair
        shapeAdded = true;
        shape = this.hackathonShapes.chair;
        break;
      case 1: //projector
        shapeAdded = true;
        shape = this.hackathonShapes.projector;
        break;
      case 2: //lamp
        shapeAdded = true;
        shape = this.hackathonShapes.lamp;
        break;
      case 3: //table
        shapeAdded = true;
        shape = this.hackathonShapes.table;

        break;
      case 4: //desk
        shapeAdded = true;
        shape = this.hackathonShapes.desk;
        break;
      case 5: //phone
        shapeAdded = true;
        shape = this.hackathonShapes.phone;

        break;
      case 6: //princess ren
        break;
      case 7: //lotion
        break;
      case 8: //podium
        shapeAdded = true;
        shape = this.hackathonShapes.podium;
        break;
      case 9: //chair and table
        shapeAdded = true;
        shape = this.hackathonShapes.chairAndTable;
        break;
      case 10: //round table
        shapeAdded = true;
        shape = this.hackathonShapes.roundTable;
        break;
    }

    if (shapeAdded) {
      shape = this.viewer.createObjectFromJSON(shape);
      defaultLayer = this.viewer.getLayerFromId("default");
      defaultLayer.addObject(shape);
    }

  };

  deleteShape() {
    this.viewer.deleteSelectedObjects()
  };
  undo() {
    this.viewer.undo();
  };
  redo() {
    this.viewer.redo();
  };

  componentDidMount() {
    let canvasWrapper = document.getElementById('canvasWrapper');
    this.viewer = new Viewer(window.innerHeight - $("#RoomViewerNav").height(), $("#canvasWrapper").width(), canvasWrapper);

    this.viewer.addLayer("layer1");

    var demoJSON = {
      elements: [
        {
          id: 1,
          type: "polygon",
          layerId: "layer1",
          fill: "#2196F3",
          points: [
            { x: 350, y: 400 },
            { x: 350, y: 800 },
            { x: 750, y: 800 },
            { x: 750, y: 400 }
          ]
        },
        {
          id: 1,
          type: "polygon",
          layerId: "layer1",
          fill: "#795548",
          points: [
            { x: 500, y: 50 },
            { x: 590, y: 50 },
            { x: 590, y: 80 },
            { x: 580, y: 80 },
            { x: 570, y: 150 },
            { x: 520, y: 150 },
            { x: 510, y: 80 },
            { x: 500, y: 80 }
          ]
        }
      ]
    };

    this.viewer.loadDrawingData(JSON.stringify(demoJSON));

    this.setState({ showLayersPanel: true });
  }

  render() {
    return (
      <div className="App">
        <Menu viewer={this.state.showLayersPanel ? this.viewer : null} />

        <div className="row">
          <div className="col s12 m4 l3">
            <ActionMenu onDelete={this.deleteShape.bind(this)} onUndo={this.undo.bind(this)} onRedo={this.redo.bind(this)} />
            <ShapeMenu addShape={this.addShape.bind(this)} />
            <LayerMenu changeLayerVisibility={this.changeLayerVisibility.bind(this)} layers={this.state.showLayersPanel ? this.viewer.layers : []} />
          </div>
          <div className="col s12 m8 l9">
            <div id="canvasWrapper">
            </div>
          </div>
        </div>


      </div>
    );
  }
}

class HackathonShapes {
  constructor() {

    this.lamp = {
      id: 1,
      type: "polygon",
      layerId: "layer1",
      fill: "rgb(255, 255, 102)",
      points: [
        { x: 15, y: 0 },
        { x: 45, y: 0 },
        { x: 60, y: 35 },
        { x: 35, y: 35 },
        { x: 45, y: 50 },
        { x: 40, y: 80 },
        { x: 20, y: 80 },
        { x: 15, y: 50 },
        { x: 25, y: 35 },
        { x: 0, y: 35 },
      ]
    };

    this.desk = {
      id: 1,
      type: "polygon",
      layerId: "layer1",
      fill: "rgb(51, 26, 0)",
      points: [
        { x: 0, y: 0 },
        { x: 200, y: 0 },
        { x: 200, y: 10 },
        { x: 190, y: 10 },
        { x: 190, y: 100 },
        { x: 180, y: 100 },
        { x: 180, y: 80 },
        { x: 20, y: 80 },
        { x: 20, y: 100 },
        { x: 10, y: 100 },
        { x: 10, y: 10 },
        { x: 0, y: 10 },
      ]
    };

    this.phone = {
      id: 1,
      type: "polygon",
      layerId: "layer1",
      fill: "rgb(31, 31, 20)",
      x: 200,
      y: 100,
      points: [
        { x: 28, y: 20 },
        { x: 30, y: 35 },
        { x: 20, y: 40 },
        { x: 10, y: 40 },
        { x: 0, y: 30 },
        { x: 0, y: 20 },
        { x: 10, y: 10 },
        { x: 50, y: 0 },
        { x: 90, y: 10 },
        { x: 100, y: 20 },
        { x: 100, y: 30 },
        { x: 90, y: 40 },
        { x: 80, y: 40 },
        { x: 70, y: 35 },
        { x: 72, y: 20 },


        { x: 50, y: 15 },
      ]
    };

    this.projector = {
      type: "group",
      layerId: "layer1",
      elements: [
        {
          type: "polygon",
          fill: "rgba(176, 197, 222, 0.75)",
          stroke: "black",
          points: [
            { x: 0, y: 0 },
            { x: 0, y: 10 },
            { x: 20, y: 10 },
            { x: 20, y: 5 },
            { x: 25, y: 10 },
            { x: 25, y: 0 },
            { x: 20, y: 5 },
            { x: 20, y: 0 },
          ]
        }
      ]
    };
    this.table = {
      type: "polygon",
      layerId: "layer1",
      fill: "rgba(176, 197, 222, 0.75)",
      stroke: "black",
      points: [
        { x: 0, y: 0 },
        { x: 100, y: 0 },
        { x: 100, y: 100 },
        { x: 0, y: 100 }
      ]
    };

    this.chair = {
      type: "group",
      layerId: "layer1",
      elements: [
        {
          type: "polygon",
          fill: "rgba(176, 197, 222, 0.75)",
          stroke: "black",
          points: [
            { x: 0, y: 0 },
            { x: 20, y: 0 },
            { x: 20, y: 60 },
            { x: 80, y: 60 },
            { x: 80, y: 100 },
            { x: 60, y: 100 },
            { x: 60, y: 80 },
            { x: 20, y: 80 },
            { x: 20, y: 100 },
            { x: 0, y: 100 },
          ]
        }
      ]
    };

    this.podium = {
      type: "polygon",
      layerId: "layer1",
      fill: "rgba(153, 102, 51, 0.75)",
      stroke: "black",
      points: [
        { x: 0, y: 0 },
        { x: 90, y: 0 },
        { x: 90, y: 30 },
        { x: 80, y: 30 },
        { x: 70, y: 100 },
        { x: 20, y: 100 },
        { x: 10, y: 30 },
        { x: 0, y: 30 }
      ]
    };

    this.chairAndTable = {
      type: "group",
      layerId: "layer1",
      elements: [
        {
          type: "circle",
          fill: "rgba(176, 197, 222, 0.75)",
          stroke: "black",
          radius: 50
        },
        {
          type: "circle",
          fill: "rgba(176, 197, 222, 0.75)",
          stroke: "black",
          x: 0,
          y: 0,
          radius: 10
        },
        {
          type: "circle",
          fill: "rgba(176, 197, 222, 0.75)",
          stroke: "black",
          x: 0,
          y: 100,
          radius: 10
        },
        {
          type: "circle",
          fill: "rgba(176, 197, 222, 0.75)",
          stroke: "black",
          x: 100,
          y: 0,
          radius: 10
        },
        {
          type: "circle",
          fill: "rgba(176, 197, 222, 0.75)",
          stroke: "black",
          x: 100,
          y: 100,
          radius: 10
        },
      ]
    };

    this.roundTable = {
      type: "circle",
      fill: "rgba(176, 197, 222, 0.75)",
      stroke: "black",
      radius: 50
    };


  }
}

export default App;
