"use strict";

import { fabric } from "fabric";
class Viewer {
    constructor(height, width, parentElement, options) {
        this._layers = [];
        this._parentElement = null;
        this._canvas = null;
        this._options = {};
        this._canvasHeight = 0;
        this._canvasWidth = 0;
        this.startPan = function (e) {
            if (e.button != 2) {
                return;
            }
            let x0 = e.screenX;
            let y0 = e.screenY;
            let continuePan = function (e) {
                let x = e.screenX;
                let y = e.screenY;
                this._canvas.relativePan({ x: x - x0, y: y - y0 });
                x0 = x;
                y0 = y;
            }.bind(this);
            let stopPan = function (e) {
                this._parentElement.removeEventListener('mousemove', continuePan);
                this._parentElement.removeEventListener('mouseup', stopPan);
            }.bind(this);
            this._parentElement.addEventListener('mousemove', continuePan);
            this._parentElement.addEventListener('mouseup', stopPan);
            this._parentElement.addEventListener('contextmenu', this.cancelMenu);
        }.bind(this);
        this.cancelMenu = function (e) {
            this.removeEventListener('contextmenu', this.cancelMenu);
            e.preventDefault();
            return false;
        };
        this.onKeyDown = function (e) {
            // this._canvas.getActiveObject();
            switch (e.keyCode) {
                case 13:
                    // Enter
                    this._canvas.deactivateAll();
                    this._canvas.renderAll();
                    break;
                case 27:
                    // Escape
                    if (this.selectedObject) {
                        this.selectedObject.resetState();
                        this._canvas.deactivateAll();
                        this._canvas.renderAll();
                    }
                    break;
                case 46:
                    // Delete
                    this.deleteSelectedObjects();
                    break;
                case 90:
                    // Z
                    if (e.ctrlKey) {
                        this.undo();
                    }
                    break;
                case 89:
                    // Y
                    if (e.ctrlKey) {
                        this.redo();
                    }
                    break;
                default:
                    break;
            }
        };
        this._canvasHeight = height;
        this._canvasWidth = width;
        this._parentElement = parentElement;
        this._options = options || {};
        this.initFabricPrototypes();
        this._canvas = this.buildCanvas("default", parentElement, this._options.viewOnly);
        this._canvas.setDimensions({ height: this._canvasHeight, width: this._canvasWidth });
        this.addLayer("default");
        this.initCanvasEvents();
    }
    get layers() {
        return this._layers;
    }
    get selectedObject() {
        return this._canvas.getActiveGroup() || this._canvas.getActiveObject();
    }
    loadDrawingData(drawingJSON) {
        let promises = [];
        let drawingObjects = JSON.parse(drawingJSON);
        let successful = false;
        if (drawingObjects) {
            if (drawingObjects.layers && drawingObjects.layers.length > 0) {
                for (let i = 0, len = drawingObjects.layers.length; i < len; i++) {
                    let layerData = drawingObjects.layers[i];
                    let newLayer = this.addLayer(layerData.id, layerData);
                }
            }
            //TODO: use Promise API somehow
            if (drawingObjects.elements && drawingObjects.elements.length > 0) {
                for (let j = 0, len2 = drawingObjects.elements.length; j < len2; j++) {
                    let elementData = drawingObjects.elements[j];
                    let newElement = this.createObjectFromJSON(elementData);
                    if (newElement) {
                        this._canvas.add(newElement);
                    }
                }
            }
            successful = true;
        }
        // Not perfect
        return successful;
    }
    addLayer(layerId, options) {
        let parent = this._parentElement;
        let newLayer = new Layer(layerId, this._canvas, options);
        this._layers.push(newLayer);
        return newLayer;
    }
    getLayerFromId(layerId) {
        let targetLayers = this._layers.filter(x => { return x.id === layerId; });
        let layer = null;
        if (targetLayers && targetLayers.length === 1) {
            layer = targetLayers[0];
        }
        return layer;
    }
    createObject(options) {
        options = options || {};
        if ('top' in options || 'x' in options) {
            options.top = options.top || options.y;
        }
        if ('left' in options || 'y' in options) {
            options.left = options.left || options.x;
        }
        let newObject = new fabric.Object(options);
        return Object;
    }
    createPolygon(points, options) {
        options = options || {};
        if ('top' in options || 'x' in options) {
            options.top = options.top || options.y;
        }
        if ('left' in options || 'y' in options) {
            options.left = options.left || options.x;
        }
        let newPolygon = new fabric.Polygon(points, options);
        return newPolygon;
    }
    /** @description Creates a Poyline from the given arguments
      * @param {array} points A list of point objects with an x and y property
      * @param {Object} [options] options to be passed to the created object
      * @returns {Object} A refrence to the newly created object
      *
      */
    createPolyline(points, options) {
        options = options || {};
        if ('top' in options || 'x' in options) {
            options.top = options.top || options.y;
        }
        if ('left' in options || 'y' in options) {
            options.left = options.left || options.x;
        }
        options.fill = options.fill || '';
        options.stroke = options.stroke || '#000000';
        let newPolyline = new fabric.Polyline(points, options);
        return newPolyline;
    }
    /** @description Creates a Line from the given arguments
      * @param {array} points A list of 4 points (x1, y1, x2, y2)
      * @param {Object} [options] options to be passed to the created object
      * @returns {Object} A refrence to the newly created object
      *
      */
    createLine(points, options) {
        options = options || {};
        if ('top' in options || 'x' in options) {
            options.top = options.top || options.y;
        }
        if ('left' in options || 'y' in options) {
            options.left = options.left || options.x;
        }
        options.fill = options.fill || '';
        options.stroke = options.stroke || '#000000';
        let newLine = new fabric.Line(points, options);
        return newLine;
    }
    createText(value, options) {
        options = options || {};
        if ('top' in options || 'x' in options) {
            options.top = options.top || options.y;
        }
        if ('left' in options || 'y' in options) {
            options.left = options.left || options.x;
        }
        let newText = new fabric.Text(value, options);
        return newText;
    }
    createPath(value, options) {
        options = options || {};
        let newPath = new fabric.Path(value, options);
        return newPath;
    }
    createEllipse(options) {
        options = options || {};
        if ('top' in options || 'x' in options) {
            options.top = options.top || options.y;
        }
        if ('left' in options || 'y' in options) {
            options.left = options.left || options.x;
        }
        let newEllipse = new fabric.Ellipse(options);
        return newEllipse;
    }
    createCircle(options) {
        options = options || {};
        if ('top' in options || 'x' in options) {
            options.top = options.top || options.y;
        }
        if ('left' in options || 'y' in options) {
            options.left = options.left || options.x;
        }
        let newCircle = new fabric.Circle(options);
        return newCircle;
    }
    // public createArc( options?: any): any {
    //   options = options || {};
    //   let newArc = new fabric.Arc(options);
    //   return newArc;
    // }
    createImage(byteArray, options) {
        options = options || {};
        if ('top' in options || 'x' in options) {
            options.top = options.top || options.y;
        }
        if ('left' in options || 'y' in options) {
            options.left = options.left || options.x;
        }
        let newImage = null;
        let createFabricImageCallback = function (oImage) {
            //TODO: always check passed in values and set these properties accordingly
            //oImage.set({
            //  scaleX: objGraphicElement.Height / oImage.height,
            //  scaleY: objGraphicElement.Width / oImage.width,
            //  left: objGraphicElement.Left,
            //  top: objGraphicElement.Top,
            //  originY: "bottom", // bottom because Y-axis is still flipped on convertion
            //  originX: "left"
            //});
            //layer.canvas.add(newImage);
            //layer.addObjects([oImage]);
            //this.imagesBuilding -= 1;
            //if (this.imagesBuilding === 0) {
            //  this.regenerateLayerImages();
            //}
        };
        newImage = new fabric.Image.fromURL("data:image/png;base64," + byteArray, createFabricImageCallback.bind(this));
        return newImage;
    }
    createGroup(objects, options) {
        options = options || {};
        if ('top' in options || 'x' in options) {
            options.top = options.top || options.y;
        }
        if ('left' in options || 'y' in options) {
            options.left = options.left || options.x;
        }
        let newGroup = new fabric.Group(objects, options);
        return newGroup;
    }
    createObjectFromJSONString(jsonString) {
        jsonString = jsonString || "";
        return this.createObjectFromJSON(JSON.parse(jsonString));
    }
    createObjectFromJSON(jsonObject) {
        let newElement = null;
        if (jsonObject) {
            switch (jsonObject.type) {
                case "group":
                    if (jsonObject.elements && jsonObject.elements.length > 0) {
                        let groupObjects = jsonObject.elements.map(this.createObjectFromJSON.bind(this));
                        newElement = this.createGroup(groupObjects, jsonObject);
                    }
                    break;
                case "polygon":
                    newElement = this.createPolygon(jsonObject.points, jsonObject);
                    break;
                case "polyline":
                    newElement = this.createPolyline(jsonObject.points, jsonObject);
                    break;
                case "line":
                    newElement = this.createLine(jsonObject.points, jsonObject);
                    break;
                case "text":
                    newElement = this.createText(jsonObject.value, jsonObject);
                    break;
                case "path":
                    newElement = this.createPath(jsonObject.path, jsonObject);
                    break;
                case "ellipse":
                    newElement = this.createEllipse(jsonObject);
                    break;
                case "circle":
                    newElement = this.createCircle(jsonObject);
                    break;
                //  case "arc":
                //    newElement = this.createArc(elementData);
                //    break;
                case "image":
                    newElement = this.createImage(jsonObject.image, jsonObject);
                    break;
                default:
                    newElement = this.createObject(jsonObject);
                    break;
            }
        }
        if (newElement && !newElement.layerId) {
            newElement.layerId = "default";
        }
        return newElement;
    }
    deleteSelectedObjects() {
        let activeObject = this._canvas.getActiveObject();
        let activeGroup = this._canvas.getActiveGroup();
        if (activeObject) {
            this._canvas.remove(activeObject);
        }
        else if (activeGroup) {
            let objectsInGroup = activeGroup.getObjects();
            this._canvas.discardActiveGroup();
            objectsInGroup.forEach((x) => {
                this._canvas.remove(x);
            });
        }
    }
    resetZoom() {
        this._canvas.setViewportTransform([1, 0, 0, 1, 0, 0]);
    }
    saveAsPng() {
        if (!fabric.Canvas.supports('toDataURL')) {
            alert('This browser doesn\'t provide means to save canvas to an image');
        }
        else {
            this._canvas.deactivateAll();
            window.open(this._canvas.toDataURL('png'));
        }
    }
    undo() {
        if (this.selectedObject) {
            this.selectedObject.undo();
        }
        this._canvas.renderAll();
        this._parentElement.focus();
    }
    ;
    redo() {
        if (this.selectedObject) {
            this.selectedObject.redo();
        }
        this._canvas.renderAll();
        this._parentElement.focus();
    }
    ;
    buildCanvas(canvasId, parentElement, staticCanvas) {
        let canvasObject;
        let canvasElement = document.createElement('canvas');
        let canvasDomElement;
        canvasElement.id = 'canvas_' + canvasId;
        parentElement.appendChild(canvasElement);
        if (staticCanvas) {
            canvasObject = new fabric.StaticCanvas(canvasElement.id);
        }
        else {
            canvasObject = new fabric.Canvas(canvasElement.id);
        }
        // canvasObject.renderOnAddRemove = true;
        canvasDomElement = canvasObject.getCanvasDomElement();
        canvasDomElement.style.zIndex = 2;
        canvasDomElement.style.position = "absolute";
        canvasDomElement.style.outline = "none";
        canvasDomElement.setAttribute("tabindex", 0); // Temporary
        return canvasObject;
    }
    initCanvasEvents() {
        let mousewheelevt = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel";
        let p = this._parentElement;
        let c = this._canvas;
        p.addEventListener(mousewheelevt, (e) => {
            let delta = e.detail ? e.detail * (-120) : e.wheelDelta;
            let x = e.offsetX || e.layerX;
            let y = e.offsetY || e.layerY;
            if (delta > 0) {
                c.zoomToPoint(new fabric.Point(x, y), c.getZoom() * 1.1);
            }
            else {
                c.zoomToPoint(new fabric.Point(x, y), c.getZoom() / 1.1);
            }
        }, false);
        p.addEventListener('mousedown', this.startPan);
        // temp for demo --
        p.addEventListener('keydown', this.onKeyDown.bind(this));
        // ----------------
        c.on('object:selected', this.objectSelectedHandler);
        c.on('object:modified', this.objectModifiedHandler);
    }
    initFabricPrototypes() {
        // Disable Object Cache for now
        fabric.Object.prototype.objectCaching = false;
        fabric.Object.prototype._setStrokeStyles = function (ctx) {
            if (this.stroke) {
                ctx.lineWidth = this.canvas.viewportTransform[0] * this.strokeWidth < 1 ? 1 / this.canvas.viewportTransform[0] : this.strokeWidth;
                ctx.lineCap = this.strokeLineCap;
                ctx.lineJoin = this.strokeLineJoin;
                ctx.miterLimit = this.strokeMiterLimit;
                ctx.strokeStyle = this.stroke.toLive ? this.stroke.toLive(ctx, this) : this.stroke;
            }
        };
        fabric.Object.prototype.onModified = function () {
            this.saveCurrentState();
        };
        fabric.Object.prototype.afterSelection = function () {
            this.setupUndoStack();
        };
        fabric.Object.prototype.setupUndoStack = function (maxSize) {
            this.undoStack = new UndoStack(maxSize);
            this.undoStack.add(this.getState());
        };
        fabric.Object.prototype.resetState = function () {
            let state;
            if (this.undoStack) {
                this.undoStack._index = 0;
                state = this.undoStack.first();
            }
            else {
                state = this.originalState;
            }
            this.setOptions(state);
            this.setCoords();
        };
        fabric.Object.prototype.getState = function (options) {
            let stateCopy = {};
            this.stateProperties.forEach(function (prop) {
                stateCopy[prop] = this.get(prop);
            }, this);
            if (options && options.stateProperties) {
                options.stateProperties.forEach(function (prop) {
                    stateCopy[prop] = this.get(prop);
                }, this);
            }
            return stateCopy;
        };
        fabric.Object.prototype.setState = function (options) {
            if (options) {
                this.setOptions(options);
                this.setCoords();
            }
        };
        fabric.Object.prototype.setupUndoStack = function (maxSize) {
            this.undoStack = new UndoStack(maxSize);
            this.undoStack.add(this.getState());
        };
        fabric.Object.prototype.saveCurrentState = function () {
            if (this.undoStack) {
                this.undoStack.add(this.getState());
            }
        };
        fabric.Object.prototype.undo = function () {
            if (this.undoStack) {
                var state = this.undoStack.stepBack();
                this.setOptions(state);
                this.setCoords();
            }
        };
        fabric.Object.prototype.redo = function () {
            if (this.undoStack) {
                var state = this.undoStack.step();
                this.setOptions(state);
                this.setCoords();
            }
        };
        fabric.Object.prototype.refreshCurrentState = function () {
            if (this.undoStack) {
                var state = this.undoStack.current();
                this.setOptions(state);
                this.setCoords();
            }
        };
        fabric.StaticCanvas.prototype.getCanvasDomElement = function () {
            return this.lowerCanvasEl;
        };
        fabric.Canvas.prototype.getCanvasDomElement = function () {
            return this.lowerCanvasEl.parentElement;
        };
    }
    objectSelectedHandler(e) {
        let selectedObject = e.target;
        if (selectedObject.afterSelection) {
            selectedObject.afterSelection();
        }
    }
    objectModifiedHandler(e) {
        let selectedObject = e.target;
        if (selectedObject.onModified) {
            selectedObject.onModified();
        }
    }
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Viewer;
class Layer {
    constructor(id, canvas, options) {
        this._visible = true;
        options = options || {};
        this._id = id;
        this._canvas = canvas;
    }
    //#region Public
    //#region Properties
    get id() {
        return this._id;
    }
    get canvas() {
        return this._canvas;
    }
    get elements() {
        return this.canvas._objects.filter((x) => { return x.layerId === this.id; });
    }
    get visible() {
        return this._visible;
    }
    set visible(value) {
        this.elements.forEach((x) => { x.visible = value; });
        this._visible = value;
        this.canvas.renderAll();
    }
    //#endregion Properties
    addObject(object) {
        this.canvas.add(object);
    }
    deleteObject(object) {
        this._canvas.remove(object);
    }
}
//#region UndoStack
class UndoStack {
    constructor(maxSize) {
        this._maxSize = 10;
        this._states = [];
        this._index = -1;
        this._maxSize = maxSize || this._maxSize;
        return this;
    }
    prev() {
        let index = this._index > 0 ? this._index - 1 : this._index;
        return this._states[index];
    }
    ;
    next() {
        let index = this._index < this._states.length - 1 ? this._index + 1 : this._index;
        return this._states[index];
    }
    ;
    current() {
        return this._states[this._index];
    }
    ;
    first() {
        return this._states[0];
    }
    ;
    stepBack() {
        // Step back if possible
        if (this._index > 0) {
            this._index -= 1;
        }
        return this._states[this._index];
    }
    ;
    step() {
        // Step forward if possible
        if (this._index < this._states.length - 1 && this._index < this._maxSize) {
            this._index += 1;
        }
        return this._states[this._index];
    }
    ;
    add(newState) {
        if (this._index < this._maxSize) {
            this._index += 1;
            // remove any elements on the stack > index
            this._states.splice(this._index);
            this._states.push(newState);
        }
        else {
            // Remove the first modified state to keep the length below the max, but keep initial state
            this._states.splice(1, 1);
            this._states.push(newState);
        }
        return this._states[this._index];
    }
    ;
}
